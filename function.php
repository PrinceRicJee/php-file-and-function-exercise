<?php

//Number 1
function fileLine($filename,$lineNum){

    if(file_exists($filename)){
        
        $file = fopen($filename,'r');

        $counter = 0;

        while(!feof($file)){

            $counter++;

            
            $lines = fgets($file);

            if($counter == $lineNum){
                return "Line $lineNum :" ." ". $lines."<br>";
            }
        
        }
    }
    else
    {
        return sprintf('file %s does not exist',$filename);
    }
}


//number 2
function appendLine($filename, $insertline, $lineNum){
    
    if(file_exists($filename)){
        
        $file = fopen($filename,'a+');

        $counter = 0;

        while(!feof($file)){

            $counter++;
            
            $lines = fgets($file);

            if($counter == $lineNum){
                fwrite($file, $insertline);
                return  $lines.$insertline;
            }
        
        } 
    }
    else
    {
        return sprintf('file %s does not exist', $filename);
    }


}



//Number 3 
function randomLiner($filename){

    if(file_exists($filename)){
        
        $newFile = strtolower($filename);

        $vowelsLength =  substr_count($newFile, 'a')
                        + substr_count($newFile, 'e')
                        + substr_count($newFile, 'i')
                        + substr_count($newFile, 'o')
                        + substr_count($newFile, 'u');
        
    
        $file = fopen($filename,'r');

        $line2 = 2;//even number of vowels

        $line3 = 3;//odd number of vowels
        
        if($vowelsLength % 2 == 0){
            
            $counter = 0;

            while(!feof($file)){

                $counter++;

                $lines = fgets($file);

                if($counter == $line2){

                     return "<br>".$lines."<br>";

                }
            }
        
        }
        else
        {
            
            $counter = 0;
             
            while(!feof($file)){

                $counter++;

                $lines = fgets($file);

                if($counter == $line3){

                     return $lines;

                }
            }


        }

    } 
    else
    {
        return sprintf('file %s does not exist !',$filename);
    }
}







//Number 4 testLoyal function
function testLoyalty($name){
  
    $len = strlen($name);
    $letterE = substr_count($name,"E");
    $letterA = substr_count($name,"A");
    $letterN = substr_count($name,"N");
    $total = $letterE + $letterA + $letterN;

    if($total >= 3){

        $product = $len * $total;

        if ($product % 6 == 0){

            return "Loyal<br>";

        }
        else
        {
            return "Di sure<br>";
        }
    }
    else
    {
        return "Di sure<br>";
    }
}

//Number 5 factorial function
function getFactorial($num){
    if($num == 1){
        return 1;
    }
    else if($num < 0){
        return abs($num);
    }
    else{

        return  $num * getFactorial($num-1);
    }
}

//Number 6
function check_prime($num)
{
   if ($num == 1){

       return 0 + "<br>";
   }

   for ($i = 2; $i <= $num / 2; $i++)
   {
      if ($num % $i == 0){

          return 0 + "<br>" ;
      }
   }
   return 1 ;
}

//Number 7 Reverse Function
function Reverse($str){

    $reverse = strrev($str);
    $reverseLength = strlen($reverse);
    
    if($reverseLength % 2 == 0){

        return '<br>'.substr($reverse, 0 , $reverseLength / 2);

    }
    else{

      return '<br>'.$reverse;

    }
}

?>